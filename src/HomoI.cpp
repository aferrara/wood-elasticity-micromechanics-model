/*
 * @file HomoI.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class HomoI implemented to compute the
 * homogenization step I of the micromechanics model for the elasticity of wood.
*/

#include "HomoI.hpp"

#define EPS 0.000001 //tolerance to exit the while loop
#define LEN 256 // buffer length for input reading

//------------------------------------------------------------------------------------
// READ INPUT FUNCTION : Read input parameters from file
// -----------------------------------------------------------------------------------

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void HomoI::input_parameter(const std::string & file) {
	double val; char name[LEN];
	int check = 0;
	Read_input my_input;
	std::ifstream fin(file); // Load file stream
	while (!fin.eof()) {
		my_input.read_line(name, &val, fin);
		std::string names; std::stringstream namess;
		namess << name; namess >> names;
		if (names=="f_h") { f_I[0]=val; check++; } //volume fraction of hemicellulose
		else if (names=="f_l") { f_I[1]=val; check++; } //volume fraction of lignin
	}
	//check if all parameters have been found or if someone is missing
	ASSERT(check == 2, "\n\nParameters not found! Check variables naming:\n - f_h = volume fraction of hemicellulose\n - f_l = volume fraction of lignin\n");
	f_I[2]=1-(f_I[0]+f_I[1]); //volume fraction of H2O+extractives
}

//------------------------------------------------------------------------------------
// ELASTIC MATRIX FUNCTION : Calculate the stiffness matrix of hemicellulose, lignin
// and H2O+extractives
// -----------------------------------------------------------------------------------
void HomoI::elastic_matrix() {

	input_parameter(dumpedinfo[0]+".inp");
	my_matrix.voigt_matrix();

	for (int i=0; i<3; i++) {
		for (int j=0; j<6; j++) {
			for (int k=0; k<6; k++) {
				c_I[i][j][k]=3*k_I[i]*my_matrix.Jv[j][k]+2*miu_I[i]*my_matrix.Kv[j][k];
			}
		}
	}
}

//------------------------------------------------------------------------------------
// PRINT RESULTS FUNCTION : Print on screen and on file the polymer stiffness matrix.
// -----------------------------------------------------------------------------------
void HomoI::print_results() {

	// Print on screen
	std::cout << "STEP I - Polymer Network stiffness matrix [GPa]:" << '\n';
	print_matrix(C_p);

	//Print on file
	std::string path_to_file = dumpedinfo[1] + "/" + dumpedinfo[0] + dumpedinfo[2];
	std::ofstream ofile(path_to_file, std::ios::out);

	ofile << "STEP I - Polymer Network stiffness matrix [GPa]:\n";
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        ofile << std::setprecision(3) << std::fixed << C_p[j][k] << '\t' << std::flush;
                }
	ofile << '\n';
	}
	ofile << '\n';

  	ofile.close();

}

// NOTATION:
// k_p = bulk modulus [GPa] of Polymer Network; miu_p = shear modulus [GPa] of Polymer Network;
// C_p = stiffness matrix of Polymer Network
// a = alpha factor of Eshelbian tensor; b =  beta factor of Eshelbian tensor;
// S_p = Eshelbian tensor;
// P_p = characteristic shape matrix for spherical inclusions

//------------------------------------------------------------------------------------
// HOMOGENEIZATION FUNCTION :  Calculate the stiffness matrix of the polymer network
// -----------------------------------------------------------------------------------
void HomoI::homogenization () {

	elastic_matrix();

	double delta=100; //minimization function difference (initial value)
	int Ntot=0, Nok=0; //counters for total number of changes of k_p and miu_p and number of "evolution success"
	double sigma=1, sigma_new=1; //scattering factor
	double c=0.85; //1/5-success rule mutation parameter
	double k_new=1, miu_new=1; //(k_p, miu_p) child
	double x; //random variable
	double G, G_new; //minimization function parent and child

	k_p=k_I[1]; //k_p parent first value assignment
	miu_p=miu_I[1]; //miu_p parent first value assignment

	while (delta>EPS) { //repeat while |Gnew-G|>tolerance

		Ntot++;
		G=min_function(k_p, miu_p); //minimization function parent
 		//time-based random generator
 		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
 		std::default_random_engine generator (seed);
		std::normal_distribution<double> distribution (0.0,1.0); //standard normal distribution
		x=distribution(generator); //random variable

		k_new=k_p+x*sigma; //k_p child
		miu_new=miu_p+(miu_p/k_p)*x*sigma; //miu_p child
		G_new=min_function(k_new, miu_new); //minimization function child

		if (G_new<G) { //evolution success
			Nok++;
			//the child becomes the parent of the next generation
			k_p=k_new;
			miu_p=miu_new;
		}
		//scattering factor mutation according to 1/5-success rule
		if (double(Nok/Ntot)<0.20) {
			sigma_new=sigma*c;
		} else if (double(Nok/Ntot)>0.20) {
			sigma_new=sigma/c;
		}
		sigma=sigma_new;

		delta=std::abs(G_new-G); //calculate |Gnew-G|
	}

}

//------------------------------------------------------------------------------------
// MINIMIZATION PROBLEM FUNCTION : Calculate the minimization function in order to
// solve the minimization problem
//------------------------------------------------------------------------------------
double HomoI::min_function(double kk, double mm) {

	double M[6][6];
	double M_inv[6][6];
	double sum_1[6][6];
	double sum_2[6][6];
	double sum_2_inv[6][6];
	double temp_1[6][6]={0};
	double temp_2[6][6]={0};
	double g_1, g_2, Gmin;

	poli_stiffness(kk, mm);
	eshelbian_sph(alpha(kk,mm), beta(kk,mm));
	shape_matrix();

	for (int i=0; i<3; i++) {
        	for (int j=0; j<6; j++) {
                	for (int k=0; k<6; k++) {
                        	M[j][k]=my_matrix.Iv[j][k]+P_p[j][k]*(c_I[i][j][k]-C_p[j][k]);

                	}
        	}
        	my_matrix.inverse(M, M_inv);

                for (int j=0; j<6; j++) {
                        for (int k=0; k<6; k++) {
                                sum_2[j][k]=f_I[i]*M_inv[j][k]+temp_2[j][k];
				 temp_2[j][k]=sum_2[j][k];
			}
		}
	}
	my_matrix.inverse(sum_2, sum_2_inv);
	
	for (int i=0; i<3; i++) {
		for (int j=0; j<6; j++) {
                        for (int k=0; k<6; k++) {
                                M[j][k]=my_matrix.Iv[j][k]+P_p[j][k]*(c_I[i][j][k]-C_p[j][k]);

                        }
                }
                my_matrix.inverse(M, M_inv);
        	for (int j=0; j<6; j++) {
                	for (int k=0; k<6; k++) {
                        	sum_1[j][k]=f_I[i]*c_I[i][j][k]*M_inv[j][k]+temp_1[j][k];
                        	temp_1[j][k]=sum_1[j][k];
			}
		}
	}

	double sum_12[6][6];
	for (int j=0; j<6; j++) {
                	for (int k=0; k<6; k++) {
                        	sum_12[j][k]=sum_1[j][k]*sum_2_inv[j][k];

                	}
        	}
        		
	g_1 = (sum_12[0][0]-2*mm*my_matrix.Kv[0][0])/(3*my_matrix.Jv[0][0])-kk;
	g_2 = (sum_12[3][3]-3*kk*my_matrix.Jv[3][3])/(2*my_matrix.Kv[0][0])-mm;
	Gmin= pow(g_1,2)+pow(g_2,2);

	return Gmin;
}

//------------------------------------------------------------------------------------
// Calculate alpha factor as a function of k_p and miu_p
// -----------------------------------------------------------------------------------
double HomoI::alpha (double kk, double mm) {
	double a;
	a=(3*kk)/(3*kk+4*mm);
	return a;
}

//------------------------------------------------------------------------------------
// Calculate beta factor as a function of k_p and miu_p
// -----------------------------------------------------------------------------------
double HomoI::beta (double kk, double mm) {
	double b;
	b=(6*(kk+2*mm))/(5*(3*kk+4*mm));
	return b;
}

//------------------------------------------------------------------------------------
// Calculate Eshelbian tensor for spherical inclusion as a function of alpha and beta
// -----------------------------------------------------------------------------------
void  HomoI::eshelbian_sph (double a, double b) {
	for (int j=0; j<6; j++) {
        	for (int k=0; k<6; k++) {
                	S_p[j][k]=a*my_matrix.Jv[j][k]+b*my_matrix.Kv[j][k];
        	}
	}

}

//------------------------------------------------------------------------------------
// Calculate the stiffness matrix of the polymer network as a function of k_p and miu_p
// -----------------------------------------------------------------------------------
void HomoI::poli_stiffness (double kk, double mm) {
	for (int j=0; j<6; j++) {
        	for (int k=0; k<6; k++) {
                	C_p[j][k]=3*kk*my_matrix.Jv[j][k]+2*mm*my_matrix.Kv[j][k];
        	}
	}
}

//------------------------------------------------------------------------------------
// SHAPE MATRIX FUNCTION : Calculate the characteristic shape matrix as a function
// of S_p and C_p
// -----------------------------------------------------------------------------------
void HomoI::shape_matrix () {
	double C_p_1[6][6];
	my_matrix.inverse(C_p, C_p_1);
	for (int j=0; j<6; j++) {
        	for (int k=0; k<6; k++) {
                	P_p[j][k]=S_p[j][k]*C_p_1[j][k];
        	}
	}

}
