/*
 * @file HomoII.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class HomoII implemented to compute the
 * homogenization step II of the micromechanics model for the elasticity of wood.
*/

#include "HomoII.hpp"

#define LEN 256 // buffer length for input reading

//------------------------------------------------------------------------------------
// READ INPUT FUNCTION : Read input parameters from file
// -----------------------------------------------------------------------------------

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void HomoII::input_parameter(const std::string & file) {
	double val; char name[LEN];
	int check = 0;	
	Read_input my_input;
	std::ifstream fin(file); // Load file stream
	while (!fin.eof()) {
		my_input.read_line(name, &val, fin);
		std::string names; std::stringstream namess;
		namess << name; namess >> names;
		if (names=="f_cry") { f_II[0]=val; check++; } //volume fraction of crystalline cellulose
		else if (names=="f_amo") { f_II[1]=val; check++; } //volume fraction of amorphous cellulose
	}
	//check if all parameters have been found or if someone is missing
	ASSERT(check == 2, "\n\nParameters not found! Check variables naming:\n - f_cry = volume fraction of crystalline cellulose\n - f_amo = volume fraction of amorphous cellulose\n");
	f_II[2]=1-(f_II[0]+f_II[1]); //volume fraction of polymer network
}

//------------------------------------------------------------------------------------
// ELASTIC MATRIX FUNCTION : Assemble the stiffness matrix of crystalline cellulose,
// amorphous cellulose and polymer matrix
// -----------------------------------------------------------------------------------
void HomoII::elastic_matrix() {
	
	input_parameter(dumpedinfo[0]+".inp");
	my_matrix.voigt_matrix();
	stepI.dump(dumpedinfo);
	stepI.homogenization();

	//crystalline cellulose stiffness matrix
	c_II[0][6][6]={0};
	c_II[0][0][0]=c_cry[0];
	c_II[0][1][1]=c_II[0][0][0];
	c_II[0][2][2]=c_cry[1];
	c_II[0][3][3]=c_cry[2];
	c_II[0][4][4]=c_II[0][3][3];
	c_II[0][5][5]=c_II[0][0][0]/2;
	
	//amorphous cellulose stiffness matrix
	for (int j=0; j<6; j++) {
		for (int k=0; k<6; k++) {	
			c_II[1][j][k]=3*k_amo*my_matrix.Jv[j][k]+2*miu_amo*my_matrix.Kv[j][k];
		}
	}

	//polymer network stiffness matrix
	for (int j=0; j<6; j++) {
		for (int k=0; k<6; k++) {
			c_II[2][j][k]=stepI.C_p[j][k];
		}
	}

}

//------------------------------------------------------------------------------------
// PRINT RESULTS FUNCTION : Print on screen and on file the resulting stiffness matrices
// -----------------------------------------------------------------------------------
void HomoII::print_results() {

	//Print results from homogeneization step I
	stepI.print_results();

	// Print on screen
	std::cout << "STEP II - Cell wall material stiffness matrix [GPa]:" << '\n';
	print_matrix(C_cwm);
		
	//Print on file
	std::string path_to_file = dumpedinfo[1] + "/" + dumpedinfo[0] + dumpedinfo[2];
	std::ofstream ofile(path_to_file, std::ios::app);
  
	ofile << "STEP II - Cell wall material stiffness matrix [GPa]:\n";
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        ofile << std::setprecision(3) << std::fixed << C_cwm[j][k] << '\t' << std::flush;
                }
	ofile << '\n';
	}
	ofile << '\n';
	
  	ofile.close();

}

// NOTATION:
// C_cwm = stiffness matrix of Cell Wall Material
// P_cwm = characteristic shape tensor for cylindrical inclusions

//------------------------------------------------------------------------------------
// HOMOGENEIZATION FUNCTION : Calculate the stiffness matrix of the cell wall material
// -----------------------------------------------------------------------------------
void HomoII::homogenization () {

	elastic_matrix();

	double M[6][6];
	double M_inv[6][6];
	double temp_1[6][6]={0};
	double temp_2[6][6]={0};
	double sum_1[6][6];
	double sum_2[6][6];
	double sum_2_inv[6][6];
	
	shape_matrix();

	for (int i=0; i<3; i++) {
        	for (int j=0; j<6; j++) {
                	for (int k=0; k<6; k++) {
                        	M[j][k]=my_matrix.Iv[j][k]+P_cwm[j][k]*(c_II[i][j][k]-c_II[2][j][k]);

                	}
        	}
        	my_matrix.inverse(M, M_inv);
        	
                for (int j=0; j<6; j++) {
                        for (int k=0; k<6; k++) {
                                sum_1[j][k]=f_II[i]*c_II[i][j][k]*M_inv[j][k]+temp_1[j][k];
				 temp_1[j][k]=sum_1[j][k];
			}
		}
	}

	for (int i=0; i<3; i++) {
		for (int j=0; j<6; j++) {
                        for (int k=0; k<6; k++) {
                                M[j][k]=my_matrix.Iv[j][k]+P_cwm[j][k]*(c_II[i][j][k]-c_II[2][j][k]);

                        }
                }
                my_matrix.inverse(M, M_inv);
        	for (int j=0; j<6; j++) {
                	for (int k=0; k<6; k++) {
                        	sum_2[j][k]=f_II[i]*M_inv[j][k]+temp_2[j][k];
                        	temp_2[j][k]=sum_2[j][k];
			}
		}
	}

	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        sum_2[j][k]+=f_II[2]*my_matrix.Iv[j][k];

                }
        }

	my_matrix.inverse(sum_2, sum_2_inv);

	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        C_cwm[j][k]=(f_II[2]*c_II[2][j][k]+sum_1[j][k])*sum_2_inv[j][k];

                }
        }

}

//------------------------------------------------------------------------------------
// SHAPE MATRIX FUNCTION : Calculate the characteristic shape tensor as a function of C_p
// -----------------------------------------------------------------------------------
void HomoII::shape_matrix () {
	
	double D =c_II[2][0][0]-c_II[2][0][1];
	std::fill(*P_cwm, *P_cwm + 6*6, 0);
	P_cwm[0][0]=1.0/8.0*(5*c_II[2][0][0]-3*c_II[2][0][1])/c_II[2][0][0]/D;
	P_cwm[1][1]=P_cwm[0][0];
	P_cwm[0][1]=-1.0/8.0*(c_II[2][0][0]+c_II[2][0][1])/c_II[2][0][0]/D;
	P_cwm[1][0]=P_cwm[0][1];
	P_cwm[3][3]=1.0/(8*D);
	P_cwm[4][4]=P_cwm[3][3];
	P_cwm[5][5]=1.0/8.0*(3*c_II[2][0][0]-c_II[2][0][1])/c_II[2][0][0]/D;

}

