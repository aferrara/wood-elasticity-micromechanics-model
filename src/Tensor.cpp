/*
 * @file Tensor.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class Tensor implemented to assemble 
 * the 4th order unit tensor and its  volumetric and deviatoric part.
*/

#include "Tensor.hpp"

//------------------------------------------------------------------------------------
// UNIT TENSOR FUNCTION : Assemble 4th-order unit tensor and calculate volumetric and
// deviatoric part
// -----------------------------------------------------------------------------------
void Tensor::fourth_tensor() {
	//Assemble 4th order unit tensor
	for (int i=0; i<3; i++) {
		for (int j=0; j<3; j++) {
        	        for (int k=0; k<3; k++) {
                	        for (int l=0; l<3; l++){
                        	        if (i==k && j==l) {
                                	        I[i][j][k][l]=1.0;
                                	} else {
                                        	I[i][j][k][l]=0.0;
                                	}
				}
                	}
        	}
	}
	//Assemble volumetric part of 4th-order unit tensor
	for (int i=0; i<3; i++) {
        	for (int j=0; j<3; j++) {
                	for (int k=0; k<3; k++) {
                        	for (int l=0; l<3; l++){
                                	if (i==j && k==l) {
                                        	J[i][j][k][l]=1.0/3.0;
                                	} else {
                                        	J[i][j][k][l]=0.0;
                                	}
                        	}
                	}
        	}
	}
	//Assemble deviatoric part of 4th-order unit tensor
	for (int i=0; i<3; i++) {
        	for (int j=0; j<3; j++) {
                	for (int k=0; k<3; k++) {
                        	for (int l=0; l<3; l++){
                                	K[i][j][k][l]=I[i][j][k][l]-J[i][j][k][l];
				}
			}
		}
	}
}

//------------------------------------------------------------------------------------
// PRINT TENSOR FUNCTION : Print 4th-order tensor on screen
//------------------------------------------------------------------------------------
void Tensor::print_tensor(double T[3][3][3][3]) {
	for (int i=0; i<3; i++) {
        	for (int j=0; j<3; j++) {
                	std::cout << "[" << i+1 << "," << j+1 << ",:,:]" << '\n';
                 	for (int k=0; k<3; k++) {
	                        for (int l=0; l<3; l++){
                                	std::cout << T[i][j][k][l] << '\t';
                        	}
                        	std::cout << '\n';
                 	}
         	}
	}

}


