/*
 * @file HomoIV.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class HomoIV implemented to compute the
 * homogenization step IV of the micromechanics model for the elasticity of wood.
*/

#include "HomoIV.hpp"

#define LEN 256 // buffer length for input reading

//------------------------------------------------------------------------------------
// READ INPUT FUNCTION : Read input parameters from file
// -----------------------------------------------------------------------------------

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void HomoIV::input_parameter(const std::string & file) {
	double val; char name[LEN];
	int check = 0;
	double rho_hw, rho_cwm, f_lum;
	Read_input my_input;
	std::ifstream fin(file); // Load file stream
	while (!fin.eof()) {
		my_input.read_line(name, &val, fin);
		std::string names; std::stringstream namess;
		namess << name; namess >> names;
		if (names=="rho_cwm") { rho_cwm=val; check++; } //density [g/cm3] of cell wall material
		else if (names=="rho_hw") { rho_hw=val; check++; } //density [g/cm3] of hardwood
		else if (names=="f_lum") { f_lum=val; check++; } //volume fraction of lumens
	}
	//check if all parameters have been found or if someone is missing
	ASSERT(check == 3, "\n\nParameters not found! Check variables naming:\n - f_lum = volume fraction of lumens\n - rho_cwm = density [g/cm3] of cell wall material\n - rho_hw = density [g/cm3] of hardwood\n");
	double f_por=1-rho_hw/rho_cwm; //volume fraction of total porosity (lumens and vessels)
	f_IV[0]=(f_por-f_lum)/(1-f_lum); //volume fraction of vessels
	f_IV[1]=1-(f_IV[0]); //volume fraction of softwood

}

//------------------------------------------------------------------------------------
// ELASTIC MATRIX FUNCTION : Assemble the stiffness matrix of sowtwood
// -----------------------------------------------------------------------------------
void HomoIV::elastic_matrix() {

	input_parameter(dumpedinfo[0]+".inp");
	my_matrix.voigt_matrix();
	//calculate elastic parameters from STEP III
	stepIII.dump(dumpedinfo);
	stepIII.homogenization();
	
	//assemble softwood stiffness matrix
	for (int j=0; j<6; j++) {
		for (int k=0; k<6; k++) {
			c_IV[j][k]=stepIII.C_sw[j][k];
		}
	}

}

//------------------------------------------------------------------------------------
// PRINT RESULTS FUNCTION : Print on screen and on file the resulting stiffness matrices
// -----------------------------------------------------------------------------------
void HomoIV::print_results() {

	//Print results from homogeneization step II
	stepIII.print_results();

	// Print on screen
	std::cout << "STEP IV - Hardwood stiffness matrix [GPa]:" << '\n';
	print_matrix(C_hw);
	std::cout << "Longitudinal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_L_hw << '\n' << std::flush;
 	std::cout << "Transversal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_T_hw << '\n' << std::flush;	
	std::cout << '\n';
		
	//Print on file
	std::string path_to_file = dumpedinfo[1] + "/" + dumpedinfo[0] + dumpedinfo[2];
	std::ofstream ofile(path_to_file, std::ios::app);
  
	ofile << "STEP IV - Hardwood stiffness matrix [GPa]:\n";
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        ofile << std::setprecision(3) << std::fixed << C_hw[j][k] << '\t' << std::flush;
                }
	ofile << '\n';
	}
	ofile << '\n';
	ofile << "Longitudinal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_L_hw << '\n' << std::flush;
 	ofile << "Transversal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_T_hw << '\n' << std::flush;
	ofile << '\n';
	
  	ofile.close();

}

// NOTATION:
// C_HW = stiffness matrix of HardWood
// P_HW = characteristic shape tensor for cylindrical inclusions

//------------------------------------------------------------------------------------
// HOMOGENEIZATION FUNCTION :  Calculate the stiffness matrix of the hardwood
// -----------------------------------------------------------------------------------
void HomoIV::homogenization () {

	elastic_matrix();

	double M[6][6];
	double M_inv[6][6];
	double sum_1[6][6];
	double sum_1_inv[6][6];
	
	shape_matrix();

       for (int j=0; j<6; j++) {
               for (int k=0; k<6; k++) {
                        	M[j][k]=my_matrix.Iv[j][k]-P_hw[j][k]*c_IV[j][k];

               }
        }

        my_matrix.inverse(M, M_inv);

        for (int j=0; j<6; j++) {
               for (int k=0; k<6; k++) {
                                sum_1[j][k]=f_IV[1]*my_matrix.Iv[j][k]+f_IV[0]*M_inv[j][k];
		}
	}
	my_matrix.inverse(sum_1, sum_1_inv);
	
	
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        C_hw[j][k]=(f_IV[1]*c_IV[j][k])*sum_1_inv[j][k];

                }
        }


	double C_hw_inv[6][6];
        my_matrix.inverse(C_hw, C_hw_inv);
        E_L_hw = 1.0/C_hw_inv[2][2];
        E_T_hw = 1.0/C_hw_inv[0][0];

}

//------------------------------------------------------------------------------------
// SHAPE MATRIX FUNCTION : Calculate the characteristic shape tensor as a function of C_sw
//------------------------------------------------------------------------------------
void HomoIV::shape_matrix () {
	
	double D = c_IV[0][0]-c_IV[0][1];
	std::fill(*P_hw, *P_hw + 6*6, 0);
	P_hw[0][0]=1.0/8.0*(5*c_IV[0][0]-3*c_IV[0][1])/c_IV[0][0]/D;
	P_hw[1][1]=P_hw[0][0];
	P_hw[0][1]=-1.0/8.0*(c_IV[0][0]+c_IV[0][1])/c_IV[0][0]/D;
	P_hw[1][0]=P_hw[0][1];
	P_hw[3][3]=1.0/(8.0*D);
	P_hw[4][4]=P_hw[3][3];
	P_hw[5][5]=1.0/8.0*(3*c_IV[0][0]-c_IV[0][1])/c_IV[0][0]/D;

}






