/*
 * @file Read_input.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class Read_input implemented to read
 * input parameters from file.
*/

#include "Read_input.hpp"

#define LEN 256 // buffer length for input reading

// Take input variable and the addresses to which assign the read name and value
void Read_input::read_line(char * nam, double * par, std::ifstream &fin) {

    char line[LEN]; // Buffer for storing a line from the file
    fin.getline(nam,LEN,'='); // Load line from file until '=' is reached
    fin.getline(line,LEN,'#'); // Load line from file until '#' character is reached
    *par = atof(line);  // Convert 'line' string to float
    fin.getline(line,LEN);  // Move the file stream pointer to the next line.

}







