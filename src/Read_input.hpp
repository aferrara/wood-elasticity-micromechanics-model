/*
 * @file Read_input.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class Read_input implemented to read
 * input parameters from file.
*/

#ifndef READINPUT
#define READINPUT

#include <fstream>

class Read_input {

friend class Homo;

public:

        void read_line(char *, double *, std::ifstream&); //read variable name and value from line

};

#endif

