/*
 * @file VoigtTensor.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class VoigtTensor implemented to assemble
 * the Voigt notation (6x6 matrix) of the 4th order unit tensor and its  volumetric
 * and deviatoric part. It also compute the co-factors, the adjoint, the determinant
 * and the inverse of a 6x6 matrix, and allocate and deallocate memory for a 6x6 matrix.
*/

#ifndef VOIGT
#define VOIGT

#include <string>
#include <iostream>
#include <cmath>
#include <cassert>
#include "Tensor.hpp"

class Tensor;

class VoigtTensor {

friend class HomoI;
friend class HomoII;
friend class HomoIII;
friend class HomoIV;
friend class TensorTestSuite;

private:

  Tensor my_tensor; //object of Tensor class (friend)
  double ** Iv,** Jv,** Kv; //voigt notation of 4th order unit tensor, volumetric part and deviatoric part

public:

  void voigt_matrix(); //convert 4th-order unit tensor, volumetric tensor and deviatoric tensor into voigt matrix
  void print_voigt(double **); //print 6x6 matrix
  void inverse(double A[6][6], double inverse[6][6]); //calculate the inverse of a 6x6 matrix
  double ** allocate(); //allocate memory for 6x6 matrix
  void deallocate(double **); //deallocate memory for 6x6 matrix

private:

  double ** voigt(double (*)[3][3][3]); //convert 4th-order tensor into voigt matrix
  double determinant(double A[6][6], int); //calculate the determinant of a 6x6 matrix
  void getCofactor (double A[6][6], double temp[6][6], int, int, int); //calculate the co-factor of a 6x6 matrix element
  void adjoint(double A[6][6], double adj[6][6]); //calculate the adjoint of a 6x6 matrix

};

#endif
