/*
 * @file HomoIII.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class HomoIII implemented to compute
 * homogenization step III of the micromechanics model for the elasticity of wood.
*/

#ifndef HOMOGENIZATIONIII
#define HOMOGENIZATIONIII

#include "Homo.hpp"
#include "HomoII.hpp"

// STEP II:
// SOFTWOOD ->  Cylindrical pores representing the cell lumens are embedded in a contiguous matrix built up by
// the cell wall material (STEP II). The behavior of such a composite material is suitably estimated by a
// Mori-Tanaka scheme.

class HomoII;

class HomoIII: public Homo {

friend class HomoIV;
friend class HomoIIITestSuite;

private:

	double c_III[6][6]; //stiffness matrix of cell wall material
	double f_III[2]; //volume fraction of lumen and cell wall material
	VoigtTensor my_matrix; //object of VoigtTensor class (friend)
	HomoII stepII; //object of HomoII class (friend)
	double P_sw[6][6]; //shape matrix of softwood
	double C_sw[6][6]; //stiffness matrix of softwood
	double E_L_sw, E_T_sw; //longitudinal and transversal elastic moduli [GPa] of sowtwood

public:

	void homogenization(); //calculate the stiffness matrix of softwood
	void print_results(); //print results on screen and on file

private:

	void input_parameter(const std::string & file); //read input parameters from file
	void shape_matrix (); //assemble shape matrix
	void elastic_matrix(); //assemble stiffness matrix of cell wall material

};

#endif
