/*
 * @file HomoIV.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class HomoIV implemented to compute
 * homogenization step IV of the micromechanics model for the elasticity of wood.
*/

#ifndef HOMOGENIZATIONIV
#define HOMOGENIZATIONIV

#include "Homo.hpp"
#include "HomoIII.hpp"

// STEP IV:
// HARDWOOD ->  Cylindrical pores representing the vessels are embedded in a contiguous matrix built up by
// the softwood-type porous material (STEP II). The behavior of such a composite material is suitably
// estimated by a Mori-Tanaka scheme.

class HomoIII;

class HomoIV: public Homo {

friend class HomoIVTestSuite;

private:

	double c_IV[6][6]; //stiffness matrix of softwood
	double f_IV[2]; //volume fraction of vessels and softwood
	VoigtTensor my_matrix; //object of VoigtTensor class (friend)
	HomoIII stepIII; //object of HomoIII class (friend)
	double P_hw[6][6]; //shape matrix of hardwood
	double C_hw[6][6]; //stiffness matrix of hardwood
	double E_L_hw, E_T_hw; //longitudinal and transversal elastic moduli [GPa] of hardwood

public:

	void homogenization(); //calculate the stiffness matrix of hardwood
	void print_results(); //print results on screen and on file

private:

	void input_parameter(const std::string & file); //read input parameters from file
	void shape_matrix (); //assemble shape matrix
	void elastic_matrix(); //assemble stiffness matrix of softwood

};

#endif
