/*
 * @file HomoIII.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class HomoIII implemented to compute the
 * homogenization step III of the micromechanics model for the elasticity of wood.
*/

#include "HomoIII.hpp"

#define LEN 256 // buffer length for input reading

//------------------------------------------------------------------------------------
// READ INPUT FUNCTION : Read input parameters from file
// -----------------------------------------------------------------------------------

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void HomoIII::input_parameter(const std::string & file) {
	double val; char name[LEN];
	int check = 0;	
	Read_input my_input;
	std::ifstream fin(file); // Load file stream
	while (!fin.eof()) {
		my_input.read_line(name, &val, fin);
		std::string names; std::stringstream namess;
		namess << name; namess >> names;
		if (names=="f_lum") { f_III[0]=val; check++; } //volume fraction of lumens
	}
	//check if all parameters have been found or if someone is missing
	ASSERT(check == 1, "\n\nParameters not found! Check variables naming:\n - f_lum = volume fraction of lumens\n");
	f_III[1]=1-(f_III[0]); //volume fraction of softwood

}

//------------------------------------------------------------------------------------
// ELASTIC MATRIX FUNCTION : Assemble the stiffness matrix of cell wall material
// -----------------------------------------------------------------------------------
void HomoIII::elastic_matrix() {
	
	input_parameter(dumpedinfo[0]+".inp");
	my_matrix.voigt_matrix();
	stepII.dump(dumpedinfo);
	stepII.homogenization();
	
	//cell wall material stiffness matrix
	for (int j=0; j<6; j++) {
		for (int k=0; k<6; k++) {
			c_III[j][k]=stepII.C_cwm[j][k];
		}
	}
	
}

//------------------------------------------------------------------------------------
// PRINT RESULTS FUNCTION : Print on screen and on file the resulting stiffness matrices
// -----------------------------------------------------------------------------------
void HomoIII::print_results() {

	//Print results from homogeneization step II
	stepII.print_results();

	// Print on screen
	std::cout << "STEP III - Softwood stiffness matrix [GPa]:" << '\n';
	print_matrix(C_sw);
	std::cout << "Longitudinal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_L_sw << '\n' << std::flush;
 	std::cout << "Transversal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_T_sw << '\n' << std::flush;
	std::cout << '\n';
	
	//Print on file
	std::string path_to_file = dumpedinfo[1] + "/" + dumpedinfo[0] + dumpedinfo[2];
	std::ofstream ofile(path_to_file, std::ios::app);
  
	ofile << "STEP III - Softwood stiffness matrix [GPa]:\n";
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        ofile << std::setprecision(3) << std::fixed << C_sw[j][k] << '\t' << std::flush;
                }
	ofile << '\n';
	}
	ofile << '\n';
	
	ofile << "Longitudinal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_L_sw << '\n' << std::flush;
 	ofile << "Transversal elastic moduli [GPa]: " << std::setprecision(3) << std::fixed << E_T_sw << '\n' << std::flush;
	ofile << '\n';
	
  	ofile.close();

}

// NOTATION:
// C_SW = stiffness matrix of SoftWood
// P_SW = characteristic shape tensor for cylindrical inclusions

//------------------------------------------------------------------------------------
// HOMOGENEIZATION FUNCTION : Calculate the stiffness matrix of the softwood
// -----------------------------------------------------------------------------------
void HomoIII::homogenization () {

	elastic_matrix();

	double M[6][6];
	double M_inv[6][6];
	double sum_1[6][6];
	double sum_1_inv[6][6];
	
	shape_matrix();

       for (int j=0; j<6; j++) {
               for (int k=0; k<6; k++) {
                        	M[j][k]=my_matrix.Iv[j][k]-P_sw[j][k]*c_III[j][k];

               }
        }
        my_matrix.inverse(M, M_inv);

        for (int j=0; j<6; j++) {
               for (int k=0; k<6; k++) {
                                sum_1[j][k]=f_III[1]*my_matrix.Iv[j][k]+f_III[0]*M_inv[j][k];
		}
	}
	my_matrix.inverse(sum_1, sum_1_inv);
	
	for (int j=0; j<6; j++) {
                for (int k=0; k<6; k++) {
                        C_sw[j][k]=(f_III[1]*c_III[j][k])*sum_1_inv[j][k];

                }
        }
        
        double C_sw_inv[6][6];
        my_matrix.inverse(C_sw, C_sw_inv);
        E_L_sw = 1.0/C_sw_inv[2][2];
        E_T_sw = 1.0/C_sw_inv[0][0];

}

//------------------------------------------------------------------------------------
// SHAPE MATRIX FUNCTION : Calculate the characteristic shape tensor as a function of C_cwm
//------------------------------------------------------------------------------------
void HomoIII::shape_matrix () {
	
	double D = c_III[0][0]-c_III[0][1];
	std::fill(*P_sw, *P_sw + 6*6, 0);
	P_sw[0][0]=1.0/8.0*(5*c_III[0][0]-3*c_III[0][1])/c_III[0][0]/D;
	P_sw[1][1]=P_sw[0][0];
	P_sw[0][1]=-1.0/8.0*(c_III[0][0]+c_III[0][1])/c_III[0][0]/D;
	P_sw[1][0]=P_sw[0][1];
	P_sw[3][3]=1.0/(8*D);
	P_sw[4][4]=P_sw[3][3];
	P_sw[5][5]=1.0/8.0*(3*c_III[0][0]-c_III[0][1])/c_III[0][0]/D;

}






