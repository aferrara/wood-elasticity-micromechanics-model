/*
 * @file HomoII.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class HomoII implemented to compute
 * homogenization step II of the micromechanics model for the elasticity of wood.
*/

#ifndef HOMOGENIZATIONII
#define HOMOGENIZATIONII

#include "Homo.hpp"
#include "HomoI.hpp"

// STEP II:
// CELL WALL MATERIAL ->  Cylindrical fiber-like aggregates of crystalline cellulose and of amorphous cellulose are
// embedded in the polymer network (STEP I). The behavior of such a composite material is suitably estimated by a
// Mori-Tanaka scheme

class HomoI;

class HomoII: public Homo {

friend class HomoIII;
friend class HomoIITestSuite;

private:

	double c_II[3][6][6]; //stiffness matrix of crystalline cellulose, amorphous cellulose and polymer matrix
	double f_II[3]; //volume fraction of crystalline cellulose, amorphous cellulose and polymer matri
	double const c_cry[3]={34.86, 167.79, 5.81}; //'universal' elements values (c_1111, c_3333, c_1313) of crystalline cellulose stiffness matrix (transverse isotropy)
	double const k_amo=5.56; //'universal' bulk modulus [GPa] of amorphous cellulose
	double const miu_amo=1.85; //'universal' shear modulus [GPa] of amorphous cellulose
	VoigtTensor my_matrix; //object of VoigtTensor class (friend)
	HomoI stepI; //object of HomoI class (friend)
	double P_cwm[6][6]; //shape matrix of cell wall material
	double C_cwm[6][6]; //stiffness matrix of cell wall material

public:

	void homogenization(); //calculate the stiffness matrix of the cell wall material
	void print_results(); //print stiffness matrix on screen and on file

private:

	void input_parameter(const std::string & file); //read input parameters from file
	void shape_matrix (); //assemble shape matrix
	void elastic_matrix(); //calculate the stiffness matrix of crystalline cellulose, amorphous cellulose and polymer matrix

};

#endif
