/*
 * @file Dump.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class Dump implented to dump the simulation
 * name, the folder path of the putput file, and the format of the output file.
*/

#ifndef DUMPCLASS
#define DUMPCLASS

#include <iostream>

class Dump {

protected:

     std::string dumpedinfo[3];

public:

     void dump(std::string * infotodump) {
 	dumpedinfo[0]=infotodump[0]; //dump simulation name (fname)
	dumpedinfo[1]=infotodump[1]; //dump saving folder path (path)
	dumpedinfo[2]=infotodump[2]; //dump output format (format)
     }

     virtual void print_results()=0; //print results

};

#endif
