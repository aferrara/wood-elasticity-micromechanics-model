/*
 * @file HomoI.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class HomoI implemented to compute
 * homogenization step I of the micromechanics model for the elasticity of wood.
*/

#ifndef HOMOGENIZATIONI
#define HOMOGENIZATIONI

#include <random>
#include <chrono>

#include "Homo.hpp"

// STEP I:
// POLYMER NETWORK ->  Hemicellulose, lignin and water are tissue-independent micromechanical phases of a polymer network.
// The disorder of the chemical components in the network and the intimate mixing of the polymers and the water motivate the use
// of a self-consistent scheme with inclusions of spherical shape.

class HomoI: public Homo {

friend class HomoII;
friend class HomoITestSuite;

private:

	double c_I[3][6][6]; //stiffness matrix of hemicellulose, lignin and H2O+extractives
	double f_I[3]; //volume fraction of hemicellulose, lignin and H2O+extractives
	double const k_I[3]={8.89, 5.00, 2.30}; //'universal' bulk modulus [GPa] of hemicellulose, lignin and H2O+extractives
	double const miu_I[3]={2.96, 2.30, 0.0}; //'universal'shear modulus [GPa] of hemicellulose, lignin and H2O+extractives
	VoigtTensor my_matrix; //object of VoigtTensor class (friend)
	double P_p[6][6]; //shape matrix of polymer network
	double C_p[6][6]; //stiffness matrix of polymer network
	double S_p[6][6]; //Eshelbian tensor for spherical inclusions
	double k_p, miu_p; //bulk and shear modulus of polymer network

public:

	void homogenization(); //calculate the stiffness matrix of the polymer network
	void print_results(); //print stiffness matrix on screen and on file

private:

	void input_parameter(const std::string & file); //read input parameters from file
	void elastic_matrix(); //Calculate the stiffness matrix of hemicellulose, lignin and H2O+extractives
	double alpha (double, double); // calculate beta factor of Eshelbian tensor
	double beta (double, double); //calculate alpha factor of Eshelbian tensor
	void eshelbian_sph (double, double); //calculate Eshelbian tensor
	void poli_stiffness (double, double); //calculate the stiffness matrix of the polymer network as a function of k_p and miu_p
	void shape_matrix (); //assemble shape matrix
	double min_function(double, double); //minimization function

};

#endif
