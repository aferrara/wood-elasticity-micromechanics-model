/*
 * @file Homo.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class Homo that is the base class of
 * 4 child classes implemented to compute each homogenization step of the
 * micromechanics model for the elasticity of wood.
*/


#ifndef HOMOGENIZATION
#define HOMOGENIZATION

#include <cmath>
#include <iostream>
#include <cassert>
#include <sstream>
#include <iomanip>

#include "Read_input.hpp"
#include "VoigtTensor.hpp"
#include "Dump.hpp"

class Read_file;
class VoigtTensor;

class Homo: public Dump {

public:

	virtual void input_parameter(const std::string &)=0; //read input parameter from file
	virtual void elastic_matrix()=0; //assemble stiffness matrix of RVE phases
	virtual void homogenization()=0; //apply homogeneization step to calculate equivalent stiffness matrix of RVE
	virtual void shape_matrix ()=0; //assemble shape matrix of RVE

	void print_matrix (double A[6][6]) { //print stiffness matrix (6x6)
                for (int j=0; j<6; j++) {
                        for (int k=0; k<6; k++) {
                                std::cout << std::setprecision(3) << std::fixed << A[j][k] << '\t' << std::flush;
                        }
                std::cout << '\n';
                }
                std::cout << '\n';
        }

};

#endif
