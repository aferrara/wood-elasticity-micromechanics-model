/*
 * @file Tensor.hpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the header file of the class Tensor implemented to assemble
 * the 4th order unit tensor and its  volumetric and deviatoric part.
*/

#ifndef TENSOR
#define TENSOR

#include <string>
#include <iostream>
#include <cmath>

class Tensor {

friend class VoigtTensor;
friend class TensorTestSuite;

private:

  double I[3][3][3][3], J[3][3][3][3], K[3][3][3][3]; //4th order unit tensor, volumetric part and deviatoric part

public:

  void fourth_tensor(); //assemble 4th-order unit tensor and calculate volumetric and deviatoric part
  void print_tensor(double T[3][3][3][3]); //print 4th-order tensor

};

#endif
