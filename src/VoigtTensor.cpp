/*
 * @file VoigtTensor.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the methods of the class VoigtTensor implemented to assemble 
 * the Voigt notation (6x6 matrix) of the 4th order unit tensor and its  volumetric 
 * and deviatoric part. It also compute the co-factors, the adjoint, the determinant
 * and the inverse of a 6x6 matrix, and allocate and deallocate memory for a 6x6 matrix.
*/

#include "VoigtTensor.hpp"

//------------------------------------------------------------------------------------
// ALLOCATE FUNCTION : Allocate memory for a 6x6 matrix
//------------------------------------------------------------------------------------
double ** VoigtTensor::allocate() {
        double** matrix;
        matrix = new double* [6];
        for (int i=0; i<6; i++) {
                matrix[i] = new double [6];
        }
        return matrix;
}

//------------------------------------------------------------------------------------
// DEALLOCATE FUNCTION : Deallocate memory for a 6x6 matrix
//------------------------------------------------------------------------------------
void VoigtTensor::deallocate(double ** matrix) {
        for (int i=0; i<6; i++) {
                delete[] matrix[i];
        }
        delete[] matrix;
}

//------------------------------------------------------------------------------------
// VOIGT NOTATION FUNCTION : Convert 4th-order tensor into Voigt matrix
// -----------------------------------------------------------------------------------
double ** VoigtTensor::voigt(double T[3][3][3][3]) {

        //top-left quarter
        double ** V;
        V=allocate();
        for (int i=0; i<3; i++) {
                for (int j=0; j<3; j++) {
                        if (i<=j) {
                                V[i][j]=T[i][i][j][j];
                        } else {
                                V[i][j]=T[j][j][i][i];
                        }
                }
        }
        //top-right and bottom-left quarter
        for (int i=0; i<3; i++) {
                V[i][3]=T[i][i][1][2];
                V[3][i]=V[i][3];
                V[i][4]=T[i][i][2][0];
                V[4][i]=V[i][4];
                V[i][5]=T[i][i][0][1];
                V[5][i]=V[i][5];
        }
        //bottom-right quarter
        V[3][3]=T[1][2][1][2];
        V[4][4]=T[2][0][2][0];
        V[5][5]=T[0][1][0][1];
        V[3][4]=T[1][2][2][0];
        V[4][3]=V[3][4];
        V[3][5]=T[1][2][0][1];
        V[5][3]=V[3][5];
        V[4][5]=T[2][0][0][1];
        V[5][4]=V[4][5];

        return V;

}

//------------------------------------------------------------------------------------
// VOIGT TENSOR : Convert 4th-order unit tensor, volumetric tensor and deviatoric
// tensor into Voigt matrix
// -----------------------------------------------------------------------------------
void VoigtTensor::voigt_matrix() {

	my_tensor.fourth_tensor();

        Iv=allocate();
        Jv=allocate();
        Kv=allocate();

        Iv=voigt(my_tensor.I);
        Jv=voigt(my_tensor.J);
        Kv=voigt(my_tensor.K);

}

//------------------------------------------------------------------------------------
// PRINT VOIGT FUNCTION : Print voigt matrix on screen
//------------------------------------------------------------------------------------
void VoigtTensor::print_voigt(double ** V) {
	for (int i=0; i<6; i++) {
        	for (int j=0; j<6; j++) {
                	std::cout << V[i][j] << '\t';
         	}
          	std::cout << '\n' << '\n';
	}

}

//------------------------------------------------------------------------------------
// INVERSE FUNCTION : Calculate the inverse of a 6x6 matrix
// -----------------------------------------------------------------------------------

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::terminate(); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

void VoigtTensor::inverse(double A[6][6], double inverse[6][6]) {
        // Find determinant of A
        int N = 6;
        double det = determinant(A, N);
        if (det == 0) {
                ASSERT(det!=0, "\n \n Singular matrix, can't find its inverse \n");
        } else {
        // Find adjoint
                double adj[6][6]; //adj=allocate();
                adjoint(A, adj);
                // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
                for (int i=0; i<N; i++) {
                        for (int j=0; j<N; j++) {
                               inverse[i][j] = adj[i][j]/det;
                        }
                }
        }
}

//------------------------------------------------------------------------------------
// Function to get adjoint of A[N][N] in adj[N][N]
// -----------------------------------------------------------------------------------
void VoigtTensor::adjoint(double A[6][6], double adj[6][6]) {
    int N=6;
    if (N == 1) {
        adj[0][0] = 1;
        return;
    }
    // temp is used to store cofactors of A[][]
    int sign = 1;
    double temp[6][6]; //temp=allocate();
    for (int i=0; i<N; i++) {
        for (int j=0; j<N; j++) {
            // Get cofactor of A[i][j]
            getCofactor(A, temp, i, j, N);
            if (determinant(temp, N-1)==0) {
            	sign=1;
            } else {
                // sign of adj[j][i] positive if sum of row and column indexes is even.
                sign = ((i+j)%2==0)? 1: -1;
            }
            // Interchanging rows and columns to get the transpose of the cofactor matrix
            adj[j][i] = (sign)*(determinant(temp, N-1));
        }
    }

}

//------------------------------------------------------------------------------------
// Function to get the determinant of A[N][N]
// -----------------------------------------------------------------------------------
double VoigtTensor::determinant(double A[6][6], int n) {
    double D = 0; // Initialize result
    //  Base case : if matrix contains single element
    if (n == 1) {
        return A[0][0];
    }
    double temp[6][6]; //temp=allocate(); // To store cofactors
    int sign = 1;  // to store sign multiplier
     // Iterate for each element of first row
    for (int f = 0; f < n; f++) {
        // Getting Cofactor of A[0][f]
        getCofactor(A, temp, 0, f, n);
        D += sign*A[0][f]*determinant(temp, n - 1);
        // terms are to be added with alternate sign
        sign = -sign;
    }
return D;
}

//------------------------------------------------------------------------------------
// Function to get co-factor of A[p][q]
// -----------------------------------------------------------------------------------
void VoigtTensor::getCofactor(double A[6][6], double temp[6][6], int p, int q, int n) {
    int i = 0, j = 0;
    // Looping for each element of the matrix
    for (int row = 0; row < n; row++) {
        for (int col = 0; col < n; col++) {
            //  Copying into temporary matrix only those element which are not in given row and column
            if (row != p && col != q) {
                temp[i][j++] = A[row][col];
                // Row is filled, so increase row index and reset col index
                if (j == n - 1) {
                    j = 0;
                    i++;
                }
            }
        }
    }

}
