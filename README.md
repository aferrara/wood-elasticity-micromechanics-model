# Wood elasticity micromechanics model

The project consists in implementing a micro-elastic model for wood, based on a four-step homogenization:
1. Polymer network
2. Cell wall material
3. Softwood
4. Hardwood