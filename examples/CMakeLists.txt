# list of examples to simulate
add_simulation(cedar_example)
add_simulation(ash_example)

configure_file(cedar_example.inp cedar_example.inp COPYONLY)
configure_file(cedar_example.run cedar_example.run COPYONLY)

configure_file(ash_example.inp ash_example.inp COPYONLY)
configure_file(ash_example.run ash_example.run COPYONLY)
