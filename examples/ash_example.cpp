/*
 * @file ash_example.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the main function to simulate the micromechanics model for 
 * the elasticity of ash wood.
*/

#include <iostream>

#include "HomoIV.hpp"

int main (int argc, char * argv []) {

	//calculate stiffness matrix of hardwood (homogeneization step IV)
	HomoIV my_homoIV; //object of class HomoIV

	//dump info about simulation name, output path and output format
	std::string temp[3];
	if(argc<4) {
    		std::cerr << "Not enough arguments:"
	      	<< " ./MMEWproject <sim-name> <output-directory> <output-format>" << std::endl;
    		return 0;
  	} else {
		temp[0]=argv[1];
		temp[1]=argv[2];
		temp[2]=argv[3];
		my_homoIV.dump(temp);
	}

	my_homoIV.homogenization(); //calculate stiffness matrix of ash
	my_homoIV.print_results(); //print results on screen and on file

return 0;

}

