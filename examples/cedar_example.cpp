/*
 * @file cedar_example.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains the main function to simulate the micromechanics model for 
 * the elasticity of cedar wood.
*/

#include <iostream>

#include "HomoIII.hpp"

int main (int argc, char * argv []) {

	//calculate stiffness matrix of softwood (homogeneization step III)
	HomoIII my_homoIII; //object of class HomoIII

	//dump info about simulation name, output path and output format
	std::string temp[3];
	if(argc<4) {
    		std::cerr << "Not enough arguments:"
	      	<< " ./MMEWproject <sim-name> <output-directory> <output-format>" << std::endl;
    		return 0;
  	} else {
		temp[0]=argv[1];
		temp[1]=argv[2];
		temp[2]=argv[3];
		my_homoIII.dump(temp);
	}

	my_homoIII.homogenization(); //calculate stiffness matrix of cedar
	my_homoIII.print_results(); //print results on screen and on file

return 0;

}

