/*
 * @file homoII.test.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains a class of methods implemented to test the class
 * "HomoII" followed by the main function to run the tests.
*/

#include <iostream>
#include <cmath>
#include <iostream>
#include "HomoII.hpp"

class HomoII;

class HomoIITestSuite {

public:

  HomoII test_homo;
  
private:

  VoigtTensor my_matrix;
  double c_cry[3]={34.86, 167.79, 5.81}; //'universal' elements values (c_1111, c_3333, c_1313) of crystalline cellulose stiffness matrix
  double k_amo=5.56; //'universal' bulk modulus [GPa] of amorphous cellulose
  double miu_amo=1.85; //'universal' shear modulus [GPa] of amorphous cellulose


private:

// Compare floating numbers to avoid internal precision errors  
bool cmp(double a, double b) {
  bool compare;
  if (std::abs(a - b) < 1e-9) { compare=true;
    } else { compare=false; }    
    return compare;
}

public:

// Test phases elastic matrix assembling
bool test_elastic_matrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.elastic_matrix();
  int count=0;
  // check amorphous cellulose stiffness matrix
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
        	if (l<3 && j<3 && l==j) {
             		if (cmp(test_homo.c_II[1][j][l],3.0*k_amo/3.0+4.0/3.0*miu_amo)) { count++;
             		} else { std::cout << "Error in amorphous cellulose stiffness matrix element " << j << "," << l << ". It should be " << 3*k_amo/3.0+4.0/3.0*miu_amo << '\n'; }
                } else if (l<3 && j<3 && l!=j) {
                       if (cmp(test_homo.c_II[1][j][l],k_amo-2.0/3.0*miu_amo)) { count++;
             		} else { std::cout << "Error in amorphous cellulose stiffness matrix element " << j << "," << l << ". It should be " << k_amo-2.0/3.0*miu_amo <<'\n'; }
                } else if (l>=3 && j>=3 && l==j) {
                	if (cmp(test_homo.c_II[1][j][l],2.0*miu_amo)) { count++;
             		} else { std::cout << "Error in amorphous cellulose stiffness matrix element " << j << "," << l << ". It should be " << 2*miu_amo << '\n'; }
                } else {
                	if (cmp(test_homo.c_II[1][j][l],0)) { count++;
             		} else { std::cout << "Error in amorphous cellulose stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
             	}
       }
  }
  // check crystalline cellulose stiffness matrix
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (j!=l) {
  			if (cmp(test_homo.c_II[0][j][l],0)) { count++;
  			} else { std::cout << "Error in polymer network stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
  		}
        }
  }
  if (cmp(test_homo.c_II[0][0][0],c_cry[0])) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 0,0. It should be " << c_cry[0] << '\n'; }
  if (cmp(test_homo.c_II[0][1][1],c_cry[0])) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 1,1. It should be " << c_cry[0] << '\n'; }
  if (cmp(test_homo.c_II[0][2][2],c_cry[1])) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 2,2. It should be " << c_cry[1] << '\n'; }
  if (cmp(test_homo.c_II[0][3][3],c_cry[2])) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 3,3. It should be " << c_cry[2] << '\n'; }
  if (cmp(test_homo.c_II[0][4][4],c_cry[2])) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 4,4. It should be " << c_cry[2] << '\n'; }
    if (cmp(test_homo.c_II[0][5][5],c_cry[0]/2.0)) { count++;
  } else { std::cout << "Error in polymer network stiffness matrix element 5,5. It should be " << c_cry[0]/2.0 << '\n'; }
  if (count<72) { a = false; }
  return a;
}

// Test characteristic shape matrix assembling
bool test_shape_matrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.elastic_matrix();
  test_homo.shape_matrix();
  int count=0;
  // check zero elements of shape matrix
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (!((j==l && j!=2) || (j==1 && l==0) || (l==1 && j==0)))  {
  			if (cmp(test_homo.P_cwm[j][l],0)) { count++;
  			} else { std::cout << "Error in shape matrix element " << j << "," << l << ". It should be 0" << '\n'; }
  		 }
        }
  }
  // check non-zero elements of shape matrix
  double D=test_homo.c_II[2][0][0]-test_homo.c_II[2][0][1];
  if (cmp(test_homo.P_cwm[0][0],1.0/8.0*(5*test_homo.c_II[2][0][0]-3*test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 0,0. It should be " << 1.0/8.0*(5*test_homo.c_II[2][0][0]-3*test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D << '\n'; }
  if (cmp(test_homo.P_cwm[1][1],1.0/8.0*(5*test_homo.c_II[2][0][0]-3*test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 1,1. It should be " << 1.0/8.0*(5*test_homo.c_II[2][0][0]-3*test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D << '\n'; }
  if (cmp(test_homo.P_cwm[0][1],-1.0/8.0*(test_homo.c_II[2][0][0]+test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 0,1. It should be " << -1.0/8.0*(test_homo.c_II[2][0][0]+test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D << '\n'; }
  if (cmp(test_homo.P_cwm[1][0],-1.0/8.0*(test_homo.c_II[2][0][0]+test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 1,0. It should be " << -1.0/8.0*(test_homo.c_II[2][0][0]+test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D << '\n'; }
  if (cmp(test_homo.P_cwm[3][3],1.0/(8*D))) { count++;
  } else { std::cout << "Error in shape matrix element 3,3. It should be " << 1.0/(8*D) << '\n'; }
  if (cmp(test_homo.P_cwm[4][4],1.0/(8*D))) { count++;
  } else { std::cout << "Error in shape matrix element 4,4. It should be " << 1.0/(8*D) << '\n'; }
  if (cmp(test_homo.P_cwm[5][5],1.0/8.0*(3*test_homo.c_II[2][0][0]-test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 5,5. It should be " << 1.0/8.0*(3*test_homo.c_II[2][0][0]-test_homo.c_II[2][0][1])/test_homo.c_II[2][0][0]/D << '\n'; } 
  if (count<36) { a = false; }
  return a;
}

// Test transversal isotropy of the cell wall material matrix
bool test_transviso_cwmmatrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.homogenization();
  int count=0;
  // check transversal isotropy of the cell wall matrix (assuming that the elastic constants are correctly calculated from homogeneization)
  // check zero elements
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (!(j==l || (j==1 && l==0) || (l==1 && j==0)))   {
  			if (cmp(test_homo.C_cwm[j][l],0)) { count++;
  			} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
  		}
        }
  }
  // check non-zero elements
  if (cmp(test_homo.C_cwm[0][0],test_homo.C_cwm[1][1])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 0,0 and 1,1. They should be equal." << '\n'; }
  if (cmp(test_homo.C_cwm[0][0],test_homo.C_cwm[2][2])) { std::cout << "Error in stiffness matrix element 0,0 or 2,2. They should be different." << '\n';
  } else { count++; }
  if (cmp(test_homo.C_cwm[3][3],test_homo.C_cwm[4][4])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 3,3 and 4,4. They should be equal." << '\n'; }
  if (cmp(test_homo.C_cwm[3][3],test_homo.C_cwm[5][5])) { std::cout << "Error in stiffness matrix element 3,3 or 5,5. They should be different." << '\n';
  } else { count++; }
  if (cmp(test_homo.C_cwm[0][1],test_homo.C_cwm[1][0])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 0,1 and 1,0. They should be equal." << '\n'; }
  if (count<36) { a = false; }
  return a;
}

};


int main() {

  HomoIITestSuite test_homo;

  std::cout << '\n' << "Start HomoII class test..." << '\n' << '\n';

  std::cout << "Test phases elastic matrix assembling: ";
  bool a = test_homo.test_elastic_matrix();
  if (a) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test characteristic shape matrix assembling: ";
  bool b = test_homo.test_shape_matrix();
  if (b) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test transversal isotropy of the cell wall material matrix: ";
  bool c = test_homo.test_transviso_cwmmatrix();
  if (c) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  // all tests passed: success
  std::cout << '\n' << "...end test." << '\n' << '\n';
  return 0; // success

}





