/*
 * @file tensor.test.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains a class of methods implemented to test the class  
 * "Tensor" and "VoigtTensor" followed by the main function to run the tests.
*/

#include <iostream>
#include <cmath>
#include <iostream>
#include "Tensor.hpp"
#include "VoigtTensor.hpp"

class Tensor;
class VoigtTensor;

class TensorTestSuite {

public:
 Tensor test_tensor;
 VoigtTensor test_matrix;
        
// Test 4th order unit tensor
bool test_unit_tensor() {
  bool a = true;
  test_tensor.fourth_tensor();  
  //check 4-th order unit tensor
  int count=0;
  for (int i=0; i<3; i++) {
  	for (int j=0; j<3; j++) {
  		for (int k=0; k<3; k++) {
  			for (int l=0; l<3; l++){
                       	if (i==k && j==l) {
                               	if (test_tensor.I[i][j][k][l]==1) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 1." << '\n'; }
                               } else {
                                    	if (test_tensor.I[i][j][k][l]==0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 0." << '\n'; }
                                    	
                            	}
			}
              	}
        }
  } 
  if (count<81) { a = false; }
  return a;
}

// Test volumetric part of 4th order unit tensor
bool test_volumetric_tensor() {
  bool a = true;
  test_tensor.fourth_tensor();  
  //check volumetric part of 4-th order unit tensor
  int count=0;
  for (int i=0; i<3; i++) {
  	for (int j=0; j<3; j++) {
  		for (int k=0; k<3; k++) {
  			for (int l=0; l<3; l++){
                       	if (i==j && k==l) {
                               	if (test_tensor.J[i][j][k][l]==1.0/3.0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 0.3333." << '\n'; }
                               } else {
                                    	if (test_tensor.J[i][j][k][l]==0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 0." << '\n'; }
                                    	
                            	}
			}
              	}
        }
  } 
  if (count<81) { a = false; }
  return a;  
}

// Test deviatoric part of 4th order unit tensor
bool test_deviatoric_tensor() {
  bool a = true;
  test_tensor.fourth_tensor();  
  //check deviatoric part of 4-th order unit tensor
  int count=0;
  for (int i=0; i<3; i++) {
  	for (int j=0; j<3; j++) {
  		for (int k=0; k<3; k++) {
  			for (int l=0; l<3; l++){
                       	if (i==j && k==l && i==k) {
                               	if (test_tensor.K[i][j][k][l]==1-1.0/3.0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 0.6667." << '\n'; }
                               } else if (i==j && k==l && (i!=k || j!=l)) {
                                    	if (test_tensor.K[i][j][k][l]==-1.0/3.0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be -0.3333." << '\n'; }
                               } else if ((i!=j || k!=l) && i==k && j==l) {
                               	if (test_tensor.K[i][j][k][l]==1) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 1." << '\n'; }
                               } else if ((i!=j || k!=l) && (i!=k || j!=l)){ 
                               	if (test_tensor.K[i][j][k][l]==0) { count++;
                               	} else { std::cout << "Error in element " << i << "," << j << "," << k << "," << l << ". It should be 0." << '\n'; }    	
                            	}
			}
              	}
        }
  }   
  if (count<81) { a = false; }
  return a; 
} 

// Test voigt unit matrix
bool test_unit_voigt() {
  bool a = true;
  test_matrix.voigt_matrix();
  //check 6x6 unit matrix
  int count=0;
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
        	if (i==j) {
             		if (test_matrix.Iv[i][j]==1) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 1." << '\n'; }
                } else {
                       if (test_matrix.Iv[i][j]==0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 1." << '\n'; }
                }
	}
  }
  if (count<36) { a = false; }
  return a;  
}

// Test volumetric part of voigt unit matrix
bool test_volumetric_voigt() {
  bool a = true;
  //check 6x6 volumetric part of unit matrix
  int count=0;
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
        	if (i<3 && j<3) {
             		if (test_matrix.Jv[i][j]==1.0/3.0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 0.3333." << '\n'; }
                } else {
                       if (test_matrix.Jv[i][j]==0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 0." << '\n'; }
                }
	}
  }
  if (count<36) { a = false; }
  return a;  
}

// Test deviatoric part of voigt unit matrix
bool test_deviatoric_voigt() {
  bool a = true;
  //check 6x6 deviatoric part of unit matrix
  int count=0;
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
        	if (i<3 && j<3 && i==j) {
             		if (test_matrix.Kv[i][j]==1-1.0/3.0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 0.6667." << '\n'; }
                } else if (i<3 && j<3 && i!=j) {
                       if (test_matrix.Kv[i][j]==-1.0/3.0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be -0.3333." << '\n'; }
                } else if (i>=3 && j>=3 && i==j) {
                	if (test_matrix.Kv[i][j]==1) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 1." << '\n'; }
                } else if ((i>=3 || j>=3) && i!=j) {
                	if (test_matrix.Kv[i][j]==0) { count++;
             		} else { std::cout << "Error in element " << i << "," << j << ". It should be 0." << '\n'; }
                }
	}
  } 
  if (count<36) { a = false; }
  return a;  
}

// Test determinant of a 6x6 matrix
bool test_determinant() {
  bool a;
  //test a diagonal matrix
  double M[6][6]={0};
  double det_M=1;
  for (int i=0; i<6; i++) {
	M[i][i]=i+1;
	det_M*=M[i][i];
  }
  double det_test = test_matrix.determinant(M, 6);
  if (det_M==det_test) { a = true;
  } else {
	a = false;
	std::cout << "   Error in computing the determinant: check 'determinant' method and 'getCofactor' method" << '\n';
  }
  return a;	
}

// Test adjoint of a 6x6 matrix
bool test_adjoint() {
  bool a = true;
  //test a diagonal matrix
  double M[6][6]={0};
  for (int i=0; i<6; i++) {
	M[i][i]=i+1;
  }
  double det_M = test_matrix.determinant(M, 6);
  double M_adj[6][6]={0};
  // assemble adjoint matrix
  for (int i=0; i<6; i++) {
	M_adj[i][i]=det_M/(i+1);
  }
  double test_adj[6][6];
  test_matrix.adjoint(M, test_adj);  
  int count=0;
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
        	if (M_adj[i][j] == test_adj[i][j]) { count++;
               } else { std::cout << "   Error in computing the adjoint matrix: check 'adjoint' method" << '\n';                     
               }
	}
  }
  if (count<36) { a = false; }
  return a;	
}

// Test inverse of a 6x6 matrix
bool test_inverse() {
  bool a = true;
  //test a diagonal matrix
  double M[6][6]={0};
  for (int i=0; i<6; i++) {
	M[i][i]=i+1;
  }
  double det_M = test_matrix.determinant(M, 6);
  double M_adj[6][6]={0};
  test_matrix.adjoint(M, M_adj);
  double M_inv[6][6];
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
	  	M_inv[i][j]=M_adj[i][j]/det_M;
  	}
  }
  double test_inv[6][6];
  test_matrix.inverse(M, test_inv);
  int count=0;
  for (int i=0; i<6; i++) {
  	for (int j=0; j<6; j++) {
        	if (M_inv[i][j] == test_inv[i][j]) { count++;
               } else { std::cout << "   Error in computing the adjoint matrix: check 'adjoint' method" << '\n';                     
               }
	}
  }
  if (count<36) { a = false; }
  return a;	
}

};


int main() {

  TensorTestSuite test_tensor;

  std::cout << '\n' << "Start Tensor class test..." << '\n' << '\n';

  std::cout << "Test 4th order unit tensor: ";
  bool a = test_tensor.test_unit_tensor();
  if (a) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << "Test volumetric part of 4th order unit tensor: ";
  bool b = test_tensor.test_volumetric_tensor();
  if (b) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << "Test deviatoric part of 4th order unit tensor: ";
  bool c = test_tensor.test_deviatoric_tensor();
  if (c) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << '\n' << "Start VoigtTensor class test..." << '\n' << '\n';
  
  std::cout << "Test unit matrix (6x6): ";
  bool d = test_tensor.test_unit_voigt();
  if (d) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << "Test volumetric part of unit matrix (6x6): ";
  bool f = test_tensor.test_volumetric_voigt();
  if (f) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test deviatoric part of unit matrix (6x6): ";
  bool g = test_tensor.test_deviatoric_voigt();
  if (g) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test computing the determinant of a (6x6) matrix: ";
  bool h = test_tensor.test_determinant();
  if (h) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << "Test computing the adjoint of a (6x6) matrix: ";
  bool l = test_tensor.test_adjoint();
  if (l) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }
  
  std::cout << "Test computing the inverse of a (6x6) matrix: ";
  bool m = test_tensor.test_inverse();
  if (m) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  // all tests passed: success
  std::cout << '\n' << "...end test." << '\n' << '\n';
  return 0; // success
  
}
