/*
 * @file homoII.test.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains a class of methods implemented to test the class
 * "HomoIII" followed by the main function to run the tests.
*/

#include <iostream>
#include <cmath>
#include <iostream>
#include "HomoIV.hpp"

class HomoIV;

class HomoIVTestSuite {

public:

  HomoIV test_homo;
  
private:

  VoigtTensor my_matrix;

private:

// Compare floating numbers to avoid internal precision errors  
bool cmp(double a, double b) {
  bool compare;
  if (std::abs(a - b) < 1e-9) { compare=true;
    } else { compare=false; }    
    return compare;
}

public:

// Test characteristic shape matrix assembling
bool test_shape_matrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.elastic_matrix();
  test_homo.shape_matrix();
  int count=0;
  // check zero elements of shape matrix
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (!((j==l && j!=2) || (j==1 && l==0) || (l==1 && j==0)))  {
  			if (cmp(test_homo.P_hw[j][l],0)) { count++;
  			} else { std::cout << "Error in shape matrix element " << j << "," << l << ". It should be 0" << '\n'; }
  		 }
        }
  }
  // check non-zero elements of shape matrix
  double D=test_homo.c_IV[0][0]-test_homo.c_IV[0][1];
  if (cmp(test_homo.P_hw[0][0],1.0/8.0*(5*test_homo.c_IV[0][0]-3*test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 0,0. It should be " << 1.0/8.0*(5*test_homo.c_IV[0][0]-3*test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D << '\n'; }
  if (cmp(test_homo.P_hw[1][1],1.0/8.0*(5*test_homo.c_IV[0][0]-3*test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 1,1. It should be " << 1.0/8.0*(5*test_homo.c_IV[0][0]-3*test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D << '\n'; }
  if (cmp(test_homo.P_hw[0][1],-1.0/8.0*(test_homo.c_IV[0][0]+test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 0,1. It should be " << -1.0/8.0*(test_homo.c_IV[0][0]+test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D << '\n'; }
  if (cmp(test_homo.P_hw[1][0],-1.0/8.0*(test_homo.c_IV[0][0]+test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 1,0. It should be " << -1.0/8.0*(test_homo.c_IV[0][0]+test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D << '\n'; }
  if (cmp(test_homo.P_hw[3][3],1.0/(8*D))) { count++;
  } else { std::cout << "Error in shape matrix element 3,3. It should be " << 1.0/(8*D) << '\n'; }
  if (cmp(test_homo.P_hw[4][4],1.0/(8*D))) { count++;
  } else { std::cout << "Error in shape matrix element 4,4. It should be " << 1.0/(8*D) << '\n'; }
  if (cmp(test_homo.P_hw[5][5],1.0/8.0*(3*test_homo.c_IV[0][0]-test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D)) { count++;
  } else { std::cout << "Error in shape matrix element 5,5. It should be " << 1.0/8.0*(3*test_homo.c_IV[0][0]-test_homo.c_IV[0][1])/test_homo.c_IV[0][0]/D << '\n'; } 
  if (count<36) { a = false; }
  return a;
}

// Test transversal isotropy of the hardwood matrix
bool test_transviso_hwmatrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.homogenization();
  int count=0;
  // check transversal isotropy of the hardwood matrix (assuming that the elastic constants are correctly calculated from homogeneization)
  // check zero elements
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (!(j==l || (j==1 && l==0) || (l==1 && j==0)))   {
  			if (cmp(test_homo.C_hw[j][l],0)) { count++;
  			} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
  		}
        }
  }
  // check non-zero elements
  if (cmp(test_homo.C_hw[0][0],test_homo.C_hw[1][1])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 0,0 and 1,1. They should be equal." << '\n'; }
  if (cmp(test_homo.C_hw[0][0],test_homo.C_hw[2][2])) { std::cout << "Error in stiffness matrix element 0,0 or 2,2. They should be different." << '\n';
  } else { count++; }
  if (cmp(test_homo.C_hw[3][3],test_homo.C_hw[4][4])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 3,3 and 4,4. They should be equal." << '\n'; }
  if (cmp(test_homo.C_hw[3][3],test_homo.C_hw[5][5])) { std::cout << "Error in stiffness matrix element 3,3 or 5,5. They should be different." << '\n';
  } else { count++; }
  if (cmp(test_homo.C_hw[0][1],test_homo.C_hw[1][0])) { count++; count++;
  } else { std::cout << "Error in stiffness matrix element 0,1 and 1,0. They should be equal." << '\n'; }
  if (count<36) { a = false; }
  return a;
}

};


int main() {

  HomoIVTestSuite test_homo;

  std::cout << '\n' << "Start HomoIV class test..." << '\n' << '\n';

  std::cout << "Test characteristic shape matrix assembling: ";
  bool a = test_homo.test_shape_matrix();
  if (a) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test transversal isotropy of the hardwood matrix: ";
  bool b = test_homo.test_transviso_hwmatrix();
  if (b) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  // all tests passed: success
  std::cout << '\n' << "...end test." << '\n' << '\n';
  return 0; // success

}





