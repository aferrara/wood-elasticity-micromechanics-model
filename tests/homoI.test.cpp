/*
 * @file homoI.test.cpp
 * @author Alessia Ferrara <aferrara@ethz.ch>
 * @date creation May 2021
 *
 * The script contains a class of methods implemented to test the class
 * "HomoI" followed by the main function to run the tests.
*/

#include <iostream>
#include <cmath>
#include <iostream>
#include "HomoI.hpp"

class HomoI;

class HomoITestSuite {

public:

  HomoI test_homo;
  
private:

  VoigtTensor my_matrix;
  double k[3]={8.89, 5.00, 2.30}; //'universal' bulk modulus [GPa] of hemicellulose, lignin and H2O+extractives
  double miu[3]={2.96, 2.30, 0.0}; //'universal'shear modulus [GPa] of hemicellulose, lignin and H2O+extractives

private:

// Compare floating numbers to avoid internal precision errors  
bool cmp(double a, double b) {
  bool compare;
  if (std::abs(a - b) < 1e-9) { compare=true;
    } else { compare=false; }    
    return compare;
}

public:

// Test phases elastic matrix assembling
bool test_elastic_matrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.elastic_matrix();
  int count=0;
  for (int i=0; i<3; i++) {
  	for (int j=0; j<6; j++) {
  		for (int l=0; l<6; l++) {
        		if (l<3 && j<3 && l==j) {
             			if (cmp(test_homo.c_I[i][j][l],3.0*k[i]/3.0+4.0/3.0*miu[i])) { count++;
             			} else { std::cout << "Error in element " << i << "," << j << "," << l << ". It should be " << 3*k[i]/3.0+4.0/3.0*miu[i] << '\n'; }
                	} else if (l<3 && j<3 && l!=j) {
                       	if (cmp(test_homo.c_I[i][j][l],k[i]-2.0/3.0*miu[i])) { count++;
             			} else { std::cout << "Error in element " << i << "," << j << "," << l << ". It should be " << k[i]-2.0/3.0*miu[i] <<'\n'; }
                	} else if (l>=3 && j>=3 && l==j) {
                		if (cmp(test_homo.c_I[i][j][l],2.0*miu[i])) { count++;
             			} else { std::cout << "Error in element " << i << "," << j << "," << l << ". It should be " << 2*miu[i] << '\n'; }
                	} else {
                		if (cmp(test_homo.c_I[i][j][l],0)) { count++;
             			} else { std::cout << "Error in element " << i << "," << j << "," << l << ". It should be 0" << '\n'; }
             		}
                }
	}
  }  
  if (count<108) { a = false; }
  return a;
}

// Test characteristic shape matrix assembling
bool test_shape_matrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  // test for elastic parameters equal to lignin
  double kk=test_homo.k_I[1]; 
  double mm=test_homo.miu_I[1];
  int count=0; 
  // check alpha and beta factor
  if (cmp(test_homo.alpha(kk, mm),(3*kk)/(3*kk+4*mm))) { count++;
  } else { std::cout << "Error in alpha factor calculation" << '\n'; }
  if (cmp(test_homo.beta(kk, mm),(6*(kk+2*mm))/(5*(3*kk+4*mm)))) { count++;
  } else { std::cout << "Error in beta factor calculation" << '\n'; }
  //check eshelbian matrix
  test_homo.eshelbian_sph(test_homo.alpha(kk, mm),test_homo.beta(kk, mm));
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
        	if (l<3 && j<3 && l==j) {
             		if (cmp(test_homo.S_p[j][l],test_homo.alpha(kk, mm)/3.0+2.0/3.0*test_homo.beta(kk, mm))) { count++;
             		} else { std::cout << "Error in eshelbian matrix element " << j << "," << l << ". It should be " << test_homo.alpha(kk, mm)/3.0+2.0/3.0*test_homo.beta(kk, mm) << '\n'; }
                } else if (l<3 && j<3 && l!=j) {
                       if (cmp(test_homo.S_p[j][l],test_homo.alpha(kk, mm)/3.0-1.0/3.0*test_homo.beta(kk, mm))) { count++;
             		} else { std::cout << "Error in eshelbian matrix element " << j << "," << l << ". It should be " << test_homo.alpha(kk, mm)/3.0-1.0/3.0*test_homo.beta(kk, mm) <<'\n'; }
                } else if (l>=3 && j>=3 && l==j) {
                	if (cmp(test_homo.S_p[j][l],test_homo.beta(kk, mm))) { count++;
             		} else { std::cout << "Error in eshelbian matrix element " << j << "," << l << ". It should be " << test_homo.beta(kk, mm) << '\n'; }
                } else {
                	if (cmp(test_homo.S_p[j][l],0)) { count++;
             		} else { std::cout << "Error in eshelbian matrix element " << j << "," << l << ". It should be 0" << '\n'; }
             	}
       }
  }
  // check stiffness matrix
  test_homo.poli_stiffness(kk, mm);
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
        	if (l<3 && j<3 && l==j) {
             		if (cmp(test_homo.C_p[j][l],3.0*kk/3.0+4.0/3.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << 3*kk/3.0+4.0/3.0*mm << '\n'; }
                } else if (l<3 && j<3 && l!=j) {
                       if (cmp(test_homo.C_p[j][l],kk-2.0/3.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << kk-2.0/3.0*mm <<'\n'; }
                } else if (l>=3 && j>=3 && l==j) {
                	if (cmp(test_homo.C_p[j][l],2.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << 2*mm << '\n'; }
                } else {
                	if (cmp(test_homo.C_p[j][l],0)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
             	}
       }
  }
  test_homo.shape_matrix();
  double C_inv[6][6];
  my_matrix.inverse(test_homo.C_p, C_inv);
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
  		if (cmp(test_homo.P_p[j][l],test_homo.S_p[j][l]*C_inv[j][l])) { count++;
             	} else { std::cout << "Error in shape matrix element " << j << "," << l << ". It should be " << test_homo.S_p[j][l]*C_inv[j][l] << '\n'; }
  	}
  }
  if (count<108) { a = false; }
  return a;
}

// Test isotropy of the polymer network matrix
bool test_isotropy_pnmatrix() {
  bool a = true;
  test_homo.dumpedinfo[0]="homo.test"; //input file
  test_homo.homogenization();
  //test_homo.print_matrix(test_homo.C_p);
  int count=0;
  // check isotropy of the polymer network matrix (assuming that k_p and miu_p are correctly calculated from min_function)
  double mm=test_homo.C_p[3][3]/2.0;
  double kk=test_homo.C_p[0][0]-4.0/3.0*mm;
  for (int j=0; j<6; j++) {
  	for (int l=0; l<6; l++) {
        	if (l<3 && j<3 && l==j) {
             		if (cmp(test_homo.C_p[j][l],3.0*kk/3.0+4.0/3.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << 3*kk/3.0+4.0/3.0*mm << '\n'; }
                } else if (l<3 && j<3 && l!=j) {
                       if (cmp(test_homo.C_p[j][l],kk-2.0/3.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << kk-2.0/3.0*mm <<'\n'; }
                } else if (l>=3 && j>=3 && l==j) {
                	if (cmp(test_homo.C_p[j][l],2.0*mm)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be " << 2*mm << '\n'; }
                } else {
                	if (cmp(test_homo.C_p[j][l],0)) { count++;
             		} else { std::cout << "Error in stiffness matrix element " << j << "," << l << ". It should be 0" << '\n'; }
             	}
       }
  }
  if (count<36) { a = false; }
  
  return a;
}

};


int main() {

  HomoITestSuite test_homoI;

  std::cout << '\n' << "Start HomoI class test..." << '\n' << '\n';

  std::cout << "Test phases elastic matrix assembling: ";
  bool a = test_homoI.test_elastic_matrix();
  if (a) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test characteristic shape matrix assembling: ";
  bool b = test_homoI.test_shape_matrix();
  if (b) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  std::cout << "Test isotropy of the polymer network matrix: ";
  bool c = test_homoI.test_isotropy_pnmatrix();
  if (c) {
    std::cerr << "succeded" << '\n';
  } else {
    std::cerr << "test failed" << '\n';
    return 1; // failure
  }

  // all tests passed: success
  std::cout << '\n' << "...end test." << '\n' << '\n';
  return 0; // success

}
